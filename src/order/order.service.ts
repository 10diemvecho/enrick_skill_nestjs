import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { AddProductToCartDto } from './dto/add-product-to-cart.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { OrderItem } from './entities/order-item.entity';
import { ProductService } from 'src/product/product.service';
import { UpdateProductDto } from 'src/product/dto/update-product.dto';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class OrderService {
  constructor(
    @Inject('PRODUCT_SERVICE') private productController: ClientProxy,
    @InjectRepository(Order)
    private orderModel: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemModel: Repository<OrderItem>,
  ) {}
  async addProductToCart(
    addProductToCartDto: AddProductToCartDto,
    customerId: number,
  ) {
    try {
      // Check if user has already had a cart
      let existedCart = await this.orderModel.findOne({
        where: {
          customerId: customerId,
          isCart: true,
        },
      });
      if (!existedCart) {
        existedCart = await this.create(
          new CreateOrderDto({
            customerId,
            isCart: true,
            orderDate: new Date(),
          }),
        );
      }

      // Check if product with this id is existed in database
      const product = await this.productController
        .send({ cmd: 'findProductById' }, addProductToCartDto.productId)
        .toPromise();
      // const product = await this.productService.findOne(
      //   addProductToCartDto.productId,
      // );
      if (!product) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'Not found product with this ID',
          },
          HttpStatus.NOT_FOUND,
        );
      }

      // Check If the order quantity is greater than amount of product in database
      if (product.quantityInStock < addProductToCartDto.quantity) {
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: 'Exceeding the number of remainning product in our store!',
          },
          HttpStatus.BAD_REQUEST,
        );
      } else {
        const remainingQuantity =
          product.quantityInStock - addProductToCartDto.quantity;
        const updateProductDto = new UpdateProductDto({
          quantityInStock: remainingQuantity,
        });
        // await this.productService.update(product.id, updateProductDto);
        const updateProductMessage = {
          productId: product.id,
          updateProductDto: updateProductDto,
        };

        await this.productController
          .send({ cmd: 'updateProduct' }, updateProductMessage)
          .toPromise();
      }

      // Check if order item for this product already existed in the database
      // if true =>> Update the quantity of product in that record
      // if false =>> Create new order item

      const existedOrderItem = await this.orderItemModel.findOne({
        where: {
          order: {
            id: existedCart.id,
          },
          productId: product.id,
        },
      });

      if (existedOrderItem) {
        const newQuantity =
          existedOrderItem.quantity + addProductToCartDto.quantity;
        const updateOrderItemDto = new UpdateOrderItemDto({
          quantity: newQuantity,
          itemPrice: product.price * newQuantity,
        });
        const updateOrderItemResult = await this.orderItemModel.update(
          existedOrderItem.id,
          updateOrderItemDto,
        );
        console.log('updated order itemssss: ', updateOrderItemResult);
      } else {
        const createOrderItemdDto = new CreateOrderItemDto({
          productId: product.id,
          quantity: addProductToCartDto.quantity,
          itemPrice: product.price * addProductToCartDto.quantity,
          order: existedCart,
        });
        const createOrderItem = await this.orderItemModel.save(
          createOrderItemdDto,
        );
        console.log('Create order item result: ', createOrderItem);
      }

      return {
        code: HttpStatus.CREATED,
        message: 'Add product to cart successfully',
      };
    } catch (error) {
      console.log('Error - Order - addProductToCart error: ', error);
      throw error;
    }
  }
  create(createOrderDto: CreateOrderDto) {
    return this.orderModel.save(createOrderDto);
  }

  findAll() {
    return `This action returns all order`;
  }

  async findOne(customerId: number) {
    let existedCart = await this.orderModel.findOne({
      where: {
        customerId: customerId,
        isCart: true,
      },
      relations: ['orderItem'],
    });
    return existedCart;
  }

  async finishOrder(customerId: number) {
    let existedCart = await this.findOne(customerId);
    if (!existedCart) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Your cart is empty!',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    let totalPrice = 0;
    for (let i = 0; i < existedCart.orderItem.length; i++) {
      totalPrice += Number(existedCart.orderItem[i].itemPrice);
    }

    await this.orderModel.update(existedCart.id, {
      isCart: false,
      totalPrice: totalPrice,
    });

    return {
      code: HttpStatus.OK,
      message: 'Finishing order sucessfully!',
    };
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  remove(id: number) {
    return `This action removes a #${id} order`;
  }
}
