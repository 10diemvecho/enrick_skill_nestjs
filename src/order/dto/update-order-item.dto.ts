import { IsNotEmpty, IsNumber, IsPhoneNumber, IsString } from 'class-validator';
import { Order } from '../entities/order.entity';

export class UpdateOrderItemDto {

  @IsNumber()
  itemPrice: number;

  @IsNumber()
  quantity: number;

  constructor(partial: Partial<UpdateOrderItemDto>) {
    Object.assign(this, partial);
  }
}
