import { IsBoolean, IsDate, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
export class CreateOrderDto {
  @IsNumber()
  @IsNotEmpty()
  customerId: number;

  @IsDate()
  orderDate: Date;

  @IsNumber()
  @IsOptional()
  quantity: number;

  @IsNumber()
  @IsOptional()
  totalPrice: number;

  @IsBoolean()
  isCart: boolean;

  constructor(partial: Partial<CreateOrderDto>) {
    Object.assign(this, partial);
  }
}
