import { IsNotEmpty, IsNumber, IsPhoneNumber, IsString } from 'class-validator';
import { Order } from '../entities/order.entity';

export class CreateOrderItemDto {
  @IsNumber()
  productId: number;

  @IsNumber()
  itemPrice: number;

  @IsNumber()
  quantity: number;

  order: Order;

  constructor(partial: Partial<CreateOrderItemDto>) {
    Object.assign(this, partial);
  }
}
