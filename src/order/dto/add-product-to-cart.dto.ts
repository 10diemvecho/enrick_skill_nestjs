import { IsNotEmpty, IsNumber, IsPhoneNumber, IsString } from 'class-validator';

export class AddProductToCartDto {
  @IsNumber()
  @IsNotEmpty()
  productId: number;

  @IsNumber()
  @IsNotEmpty()
  quantity: number;

  constructor(partial: Partial<AddProductToCartDto>) {
    Object.assign(this, partial);
  }
}
