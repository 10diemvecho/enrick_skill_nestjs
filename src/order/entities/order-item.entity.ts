
import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './order.entity';
import { Product } from 'src/product/entities/product.entity';
import { IsNumber } from 'class-validator';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Order, (order) => order.orderItem)
  order: Order;

  // @ManyToOne(() => Product, (product) => product.orderItem)
  // product: Product;
  @Column()
  @IsNumber()
  productId: number;

  @Column()
  @Column('numeric', { precision: 10, scale: 2, unsigned: true })
  itemPrice: number;

  @Column()
  @Column({ type: 'integer', unsigned: true })
  quantity: number;
}
