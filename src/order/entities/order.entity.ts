import {
  IsBoolean,
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsPhoneNumber,
} from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Customer } from 'src/customer/entities/customer.entity';
import { OrderItem } from './order-item.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsDate()
  orderDate: Date;

  @Column('numeric', { precision: 10, scale: 2, default: 0 })
  totalPrice: number;

  @Column()
  @IsNotEmpty()
  @IsBoolean()
  isCart: boolean;

  @Column()
  @IsNumber()
  customerId: number;

  // @OneToOne(() => Customer)
  // @JoinColumn()
  // customer: Customer;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItem: OrderItem[];
}
