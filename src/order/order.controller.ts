import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  Put,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { AuthGuard } from '../auth/auth.guard';
import { AddProductToCartDto } from './dto/add-product-to-cart.dto';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @UseGuards(AuthGuard)
  @Post('/add-product-to-cart')
  addProductToCart(
    @Req() request: Request,
    @Body() addProductToCartDto: AddProductToCartDto,
  ) {
    const user = request['user'];
    console.log('User From request: ', user);

    const customerId = user.customerId;
    return this.orderService.addProductToCart(addProductToCartDto, customerId);
  }

  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.orderService.create(createOrderDto);
  }

  @Get()
  findAll() {
    return this.orderService.findAll();
  }

  @UseGuards(AuthGuard)
  @Get('get-cart')
  findOne(@Req() request: Request) {
    const user = request['user'];
    console.log('User: ', user);
    const customerId = user.customerId;
    return this.orderService.findOne(customerId);
  }

  @UseGuards(AuthGuard)
  @Put('finish-order')
  finishOrder(@Req() request: Request) {
    const user = request['user'];
    console.log('User: ', user);
    const customerId = user.customerId;
    return this.orderService.finishOrder(customerId);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.orderService.update(+id, updateOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderService.remove(+id);
  }
}
