import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Manufacturer } from './entities/manufacturer.entity';
import { ManufacturerService } from './manufacturer.service';

@Controller('manufacturers')
export class ManufacturerController {
  constructor(private readonly manufacturerService: ManufacturerService) {}

  @Get()
  findAll(): Promise<Manufacturer[]> {
    return this.manufacturerService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Manufacturer> {
    return this.manufacturerService.findOne(+id);
  }

  @Post()
  create(@Body() manufacturer: Manufacturer): Promise<Manufacturer> {
    return this.manufacturerService.create(manufacturer);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() manufacturer: Manufacturer,
  ): Promise<Manufacturer> {
    return this.manufacturerService.update(+id, manufacturer);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.manufacturerService.remove(+id);
  }
}
