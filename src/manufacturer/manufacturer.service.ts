
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Manufacturer } from './entities/manufacturer.entity';

@Injectable()
export class ManufacturerService {
  constructor(
    @InjectRepository(Manufacturer)
    private readonly manufacturerRepository: Repository<Manufacturer>,
  ) {}

  async findAll(): Promise<Manufacturer[]> {
    return this.manufacturerRepository.find();
  }

  findOne(id: number) {
    return this.manufacturerRepository.findOne({
      where: {
        id: Number(id) || 0,
      },
    });
  }

  async create(manufacturer: Manufacturer): Promise<Manufacturer> {
    return this.manufacturerRepository.save(manufacturer);
  }

  async update(id: number, manufacturer: Manufacturer): Promise<Manufacturer> {
    await this.manufacturerRepository.update(id, manufacturer);
    return this.manufacturerRepository.findOne({
      where: {
        id: Number(id) || 0,
      },
    });
  }

  async remove(id: number): Promise<void> {
    await this.manufacturerRepository.delete(id);
  }
}
