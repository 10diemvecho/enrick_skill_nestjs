import { IsNotEmpty, IsString } from 'class-validator';
import { Product } from 'src/product/entities/product.entity';

import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Manufacturer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsString()
  @IsNotEmpty()
  manufacturerName: string;

  @Column()
  @IsString()
  @IsNotEmpty()
  country: string;

  @OneToMany(() => Product, (product) => product.manufacturer)
  product: Product[];
}
