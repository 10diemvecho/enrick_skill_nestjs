
import {
  IsNotEmpty,
  IsString,
} from 'class-validator';
import { User } from '../../users/entities/users.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Color {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsString()
  @IsNotEmpty()
  colorName: string;

}
