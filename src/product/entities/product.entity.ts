import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Manufacturer } from 'src/manufacturer/entities/manufacturer.entity';
import { OrderItem } from 'src/order/entities/order-item.entity';

import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsString()
  @IsNotEmpty()
  name: string;

  @Column()
  @IsString()
  @IsNotEmpty()
  type: string; // BreakCue, JumpCue, NormalCue, Other

  @Column()
  @IsString()
  @IsNotEmpty()
  model: string;

  @Column()
  @IsNotEmpty()
  @Column('numeric', { precision: 10, scale: 2, unsigned: true })
  price: number;

  @Column()
  @IsNumber()
  quantityInStock: number;

  @ManyToOne(() => Manufacturer, (manufacturer) => manufacturer.product)
  manufacturer: Manufacturer;

  // @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  // orderItem: OrderItem[];
}
