import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { Product } from './entities/product.entity';
import { ManufacturerService } from 'src/manufacturer/manufacturer.service';
import { FindAllProductDto } from './dto/find-all-product-dto';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productModel: Repository<Product>,
    private manufacturerService: ManufacturerService,
  ) {}

  async findProductById(id: number): Promise<Product> {
    return this.findOne(id);
  }

  async updateProduct(data: {
    productId: number;
    updateProductDto: UpdateProductDto;
  }) {  
    const { productId, updateProductDto } = data;

    const product = await this.findOne(productId);

    if (!product) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'Not found product with this ID',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    Object.assign(product, updateProductDto);
    await this.productModel.save(product);
  }

  async create(createProductDto: CreateProductDto) {
    const manufacturer = await this.manufacturerService.findOne(
      createProductDto.manufacturerId,
    );
    if (!manufacturer) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error:
            'Not found manufacturer. Please provide information about this manufacturer before creating its products.',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    createProductDto.manufacturer = manufacturer;

    return this.productModel.save(createProductDto);
  }

  async findAll(findAllProductDto: FindAllProductDto) {
    const take = Number(findAllProductDto.size);
    const skip = (Number(findAllProductDto.page) - 1) * take;
    const query = {
      take: take,
      skip: skip,
      where: {},
      relations: ['manufacturer'],
    };

    if (findAllProductDto.name) {
      query.where['name'] = ILike(`%${findAllProductDto.name}%`);
    }
    if (findAllProductDto.type) {
      query.where['type'] = findAllProductDto.type;
    }
    if (findAllProductDto.model) {
      query.where['model'] = findAllProductDto.model;
    }

    const result = await this.productModel.findAndCount(query);
    const response = {
      totalRecords: result[1],
      data: result[0],
      currentPage: Number(findAllProductDto.page),
      totalPages: Math.ceil(Number(result[1]) / Number(findAllProductDto.size)),
    };
    return response;
  }

  findOne(id: number) {
    return this.productModel.findOne({
      where: {
        id: Number(id) || 0,
      },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    try {
      const product = await this.findOne(id);

      if (!product) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error: 'Not found product with this ID.',
          },
          HttpStatus.NOT_FOUND,
        );
      }
      if (updateProductDto.manufacturerId) {
        const manufacturer = await this.checkExistManufacturer(
          updateProductDto.manufacturerId,
        );
        updateProductDto.manufacturer = manufacturer;
        delete updateProductDto.manufacturerId;
      }

      console.log('updateProductDto: ', updateProductDto);

      return this.productModel.update(id, updateProductDto);
    } catch (error) {
      throw error;
    }
  }

  async checkExistManufacturer(manufacturerId: any) {
    try {
      const manufacturer = await this.manufacturerService.findOne(
        manufacturerId,
      );
      if (!manufacturer) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_FOUND,
            error:
              'Not found manufacturer. Please provide information about this manufacturer before creating its products.',
          },
          HttpStatus.NOT_FOUND,
        );
      }
      return manufacturer;
    } catch (error) {
      throw error;
    }
  }

  remove(id: number) {
    return `This action removes a #${id} product`;
  }
}
