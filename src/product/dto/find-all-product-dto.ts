import { IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString } from 'class-validator';
import { Manufacturer } from 'src/manufacturer/entities/manufacturer.entity';

export class FindAllProductDto {
  @IsNumberString()
  @IsNotEmpty()
  page: string;

  @IsNumberString()
  @IsNotEmpty()
  size: string;

  @IsString()
  @IsOptional()
  model: string;

  @IsString()
  @IsOptional()
  type: number;

  @IsString()
  @IsOptional()
  name: number;
}
