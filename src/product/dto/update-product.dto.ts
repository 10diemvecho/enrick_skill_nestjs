import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { Manufacturer } from 'src/manufacturer/entities/manufacturer.entity';

export class UpdateProductDto {
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  name: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  type: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  model: string;

  @IsNotEmpty()
  @IsNumber()
  @IsOptional()
  price: number;

  @IsNumber()
  @IsNotEmpty()
  @IsOptional()
  quantityInStock: number;

  @IsNumber()
  @IsNotEmpty()
  @IsOptional()
  manufacturerId: number;

  manufacturer: Manufacturer;

  constructor(partial: Partial<UpdateProductDto>) {
    Object.assign(this, partial);
  }
}
