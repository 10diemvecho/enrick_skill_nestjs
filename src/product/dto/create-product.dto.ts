import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Manufacturer } from 'src/manufacturer/entities/manufacturer.entity';

export class CreateProductDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  type: string;

  @IsString()
  @IsNotEmpty()
  model: string;

  @IsNotEmpty()
  @IsNumber()
  price: number;

  @IsNumber()
  @IsNotEmpty()
  quantityInStock: number;

  @IsNumber()
  @IsNotEmpty()
  manufacturerId: number;

  manufacturer: Manufacturer;
}
