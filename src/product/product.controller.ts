import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards } from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { FindAllProductDto } from './dto/find-all-product-dto';
import { MessagePattern } from '@nestjs/microservices';
import { Product } from './entities/product.entity';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}
  // Admin API

  @Post('create')
  create(@Body() createProductDto: CreateProductDto) {
    return this.productService.create(createProductDto);
  }

  @Get('find-all')
  findAll(@Query() finAllProductDto: FindAllProductDto) {
    return this.productService.findAll(finAllProductDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productService.findOne(+id);
  }

  @Patch('update/:id')
  update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
    return this.productService.update(+id, updateProductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productService.remove(+id);
  }

  @MessagePattern({ cmd: 'findProductById' })
  async findProductById(id: number): Promise<Product> {
    return this.productService.findOne(id);
  }

  @MessagePattern({ cmd: 'updateProduct' })
  async updateProduct(data) : Promise<any>{
    return this.productService.updateProduct(data);
  }
}
