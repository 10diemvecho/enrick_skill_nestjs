import { IsNotEmpty, IsNumber, IsPhoneNumber, IsString } from "class-validator";
import { User } from "src/users/entities/users.entity";

export class CreateCustomerDto {
  @IsString()
  @IsNotEmpty()
  email: string;

  @IsPhoneNumber()
  phone: string;

  @IsString()
  address: string;

  user: User;

  constructor(partial: Partial<CreateCustomerDto>) {
    Object.assign(this, partial);
  }
}


