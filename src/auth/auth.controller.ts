import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  Request,
  UseGuards
} from '@nestjs/common';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { ValidateRegistorUserDto } from 'src/users/dto/validate-register-user.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() signInDto: Record<string, any>) {
    return this.authService.signIn(signInDto.username, signInDto.password);
  }

  @HttpCode(HttpStatus.OK)
  @Post('register')
  register(@Body() validateRegisterUserDto: ValidateRegistorUserDto) {
    return this.authService.register(validateRegisterUserDto);
  }

  @UseGuards(AuthGuard)
  @Get('profile')
  getProfile(@Query('id') id: string) {
    return this.authService.getProfile(id);
  }

}