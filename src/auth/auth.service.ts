import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ValidateRegistorUserDto } from 'src/users/dto/validate-register-user.dto';
import * as bcrypt from 'bcrypt';
import { CustomerService } from 'src/customer/customer.service';
import { CreateCustomerDto } from 'src/customer/dto/create-customer.dto';

@Injectable()
export class AuthService {
  constructor(
    private customerService: CustomerService,
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async getProfile(id: string) {
    const user = await this.usersService.findById(id);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'Not found user with this information',
        },
        HttpStatus.NOT_FOUND,
      );
    }

    return {
      userId: user.id,
      role: user.role,
    };
  }

  async signIn(username: string, pass: string) {
    const expiresIn = '30d';
    const user = await this.usersService.findOne(username.toLowerCase());
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: 'Invalid username or password! Please try again!',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }

    const isMatch = await bcrypt.compare(pass, user?.password);
    if (!isMatch) {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: 'Invalid password! Please try again!',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
    console.log('userLoginnn: ', user);
    const payload = {
      sub: user.id,
      username: user.username,
      role: user.role,
      c: user.customer.id,
      email: user.customer.email,
      phone: user.customer.phone,
      address: user.customer.address,
    };
    const accessToken = await this.jwtService.signAsync(payload, { expiresIn });

    return {
      access_token: accessToken,
    };
  }

  async register(validateRegistorUserDto: ValidateRegistorUserDto) {
    // Check existed user in system
    const user = await this.usersService.findOne(
      validateRegistorUserDto.username,
    );
    if (user) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error:
            'This username is already existed in the system! Please choose another one!',
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    // Create new user in database
    const { username, password, email, address, phone } =
      validateRegistorUserDto;
    const generatedPassword = await this.generatePassword(password);

    const createUserResult = await this.usersService.create(
      new CreateUserDto({
        username: username.toLowerCase(),
        password: generatedPassword,
        role: '1',
      }),
    );

    console.log('Create User Result: ', createUserResult);

    const createCustomerResult = await this.customerService.create(
      new CreateCustomerDto({
        email: email,
        user: createUserResult,
        address: address,
        phone: phone,
      }),
    );

    console.log('Create additional user information: ', createCustomerResult);

    return {
      code: HttpStatus.ACCEPTED,
      message:
        'Register account successfully! From now on, you will be able to login to our system!',
    };
  }

  async generatePassword(password) {
    const saltOrRounds = 10;
    return await bcrypt.hash(password, saltOrRounds);
  }
}