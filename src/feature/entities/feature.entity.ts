import { Type } from 'class-transformer';
import { IsDefined, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Role } from 'src/role/entities/role.entity';

import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Feature {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsString()
  @IsNotEmpty()
  featureName: string;

  @IsDefined()
  @Type(() => Role)
  // @ValidateNested({ each: true })
  @ManyToMany(() => Role, { cascade: true })
  @JoinTable()
  role: Role;
}
