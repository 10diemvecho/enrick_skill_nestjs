import { Type } from 'class-transformer';
import { IsDefined, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Role } from 'src/role/entities/role.entity';

import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Feature } from './feature.entity';

@Entity()
export class RoleFeature {
  @PrimaryGeneratedColumn()
  id: number;

  @IsDefined()
  @ValidateNested({ each: true })
  @ManyToMany(() => Feature, { cascade: true })
  @JoinTable()
  role: Role;
}
