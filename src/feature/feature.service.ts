import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Feature } from './entities/feature.entity';

@Injectable()
export class FeatureService {
  constructor(
    @InjectRepository(Feature)
    private readonly featureRepository: Repository<Feature>,
  ) {}

  async findAll(): Promise<Feature[]> {
    return this.featureRepository.find();
  }

  async findOne(id: number): Promise<Feature> {
    return this.featureRepository.findOne({
      where: {
        id: id
      }
    });
  }

  async create(feature: Feature): Promise<Feature> {
    return this.featureRepository.save(feature);
  }

  async update(id: number, feature: Feature): Promise<Feature> {
    await this.featureRepository.update(id, feature);
    return this.featureRepository.findOne({
      where: {
        id: id
      }
    });
  }

  async remove(id: number): Promise<void> {
    await this.featureRepository.delete(id);
  }
}
