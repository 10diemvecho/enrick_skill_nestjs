import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Feature } from './entities/feature.entity';
import { FeatureService } from './feature.service';

@Controller('features')
export class FeatureController {
  constructor(private readonly featureService: FeatureService) {}

  @Get()
  findAll(): Promise<Feature[]> {
    return this.featureService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<Feature> {
    return this.featureService.findOne(+id);
  }

  @Post()
  create(@Body() feature: Feature): Promise<Feature> {
    return this.featureService.create(feature);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() feature: Feature): Promise<Feature> {
    return this.featureService.update(+id, feature);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.featureService.remove(+id);
  }
}
