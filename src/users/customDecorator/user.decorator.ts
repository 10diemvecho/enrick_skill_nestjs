import {registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface} from 'class-validator';

function IsFPTEmail(properties ?: string, validationOptions ?: ValidationOptions){
  return (object: Object, propertyName: string) => {
    return registerDecorator({
      name: "IsFPTEmail",
      target: object.constructor,
      propertyName: propertyName,
      validator: { validate(value: string, argument: ValidationArguments){
        return value.endsWith("@fpt.com");
      }},
      options: {
        message: "Email must be in FPT"
      }
    });
  };
}

function IsComplexPassword(properties ?: string, validationOptions ?: ValidationOptions){
  return (object: Object, propertyName: string) => {
    return registerDecorator({
      name: "IsComplexPassword",
      target: object.constructor,
      propertyName: propertyName,
      validator: { 
        validate(value: string, argument: ValidationArguments){
        // const isWhitespace = new RegExp("/^(?=.@*\s)/");
        const isContainsUppercase = /^(?=.*[A-Z])/;
        const isContainsLowercase = /^(?=.*[a-z])/;
        const isContainsNumber = /^(?=.*[0-9])/;
        const isValidLength = /^.{10,16}$/;
        console.log("Value: ", value)
        console.log(/^.{10,16}$/.test(value), /^(?=.*[0-9])/.test(value), /^(?=.*[a-z])/.test(value), /^(?=.*[A-Z])/.test(value))
        return isContainsUppercase.test(value) && isContainsLowercase.test(value)
          && isContainsNumber.test(value) && isValidLength.test(value)
      }},
      options: {
        message: "Password must be complex enough"
      }
    });
  };
}


function IsMatch(property: string, validationOptions?: ValidationOptions) {
    return (object: any, propertyName: string) => {
        registerDecorator({
            name: "IsMatch",
            target: object.constructor,
            propertyName,
            // options: validationOptions,
            constraints: [property],
            validator: { 
              validate(value: string, argument: ValidationArguments){
                const [relatedPropertyName] = argument.constraints;
                const relatedValue = (argument.object as any)[relatedPropertyName];
                return value === relatedValue;
            }},
            options: {
              message: "Confirm password must be matched with password!"
            }
        });
    };
}

export {
  IsComplexPassword,
  IsFPTEmail,
  IsMatch
}