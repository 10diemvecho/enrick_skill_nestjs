import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/users.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userModel: Repository<User>
  ){}

  async findOne(username: string): Promise<User | undefined> {
    return this.userModel.findOne({ 
      where: {
        username: username
      },
      relations: ["customer"]
    });
  }

  async findById(id: string): Promise<User | undefined> {
      return this.userModel.findOne({ 
      where: {
        id: Number(id) || 0
      }
    });
  }

  create(createUserDto: CreateUserDto) {
    return this.userModel.save(createUserDto);
  }
}