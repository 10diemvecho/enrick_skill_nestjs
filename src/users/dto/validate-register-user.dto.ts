
import { IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';
import { Transform } from "class-transformer";

import { IsComplexPassword, IsMatch } from '../customDecorator/user.decorator';
export class ValidateRegistorUserDto {
  @IsString({ message: 'Username must be a string' })
  username: string;

  @IsString()
  @IsNotEmpty()
  @IsComplexPassword()
  password: string;

  @IsString()
  @IsNotEmpty()
  @IsMatch('password')
  confirmPassword: string;

  // @IsString()
  // fullname: string;

  @IsString()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsPhoneNumber()
  phone: string;

  @IsString()
  @IsNotEmpty()
  address: string;
}
