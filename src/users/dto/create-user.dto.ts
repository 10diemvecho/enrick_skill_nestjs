
import { IsNotEmpty, IsString } from 'class-validator';
import { IsComplexPassword, IsMatch } from '../customDecorator/user.decorator';
export class CreateUserDto{
  @IsString({ message: "Username must be a string"})
  username: string;

  @IsString({ message: "Password must be a string"})
  @IsNotEmpty()
  @IsComplexPassword()
  password: string;

  @IsString({ message: "Role is required"})
  role: string;

  constructor(partial: Partial<CreateUserDto>) {
    Object.assign(this, partial);
  }

}


