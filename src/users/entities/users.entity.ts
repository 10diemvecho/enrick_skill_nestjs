import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Unique,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';
import { IsComplexPassword, IsMatch } from '../customDecorator/user.decorator';
import { Customer } from '../../customer/entities/customer.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true,
  })
  @IsString({ message: 'Username must be a string' })
  username: string;

  @Column()
  @IsString({ message: 'Password must be a string' })
  @IsNotEmpty()
  @IsComplexPassword()
  password: string;

  @Column()
  @IsString()
  role: string;

  @OneToOne(() => Customer, (customer) => customer.user, { cascade: true })
  customer: Customer;
}
