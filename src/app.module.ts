import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/users.entity';
import { CustomerModule } from './customer/customer.module';
import { Customer } from './customer/entities/customer.entity';
import { RoleModule } from './role/role.module';
import { Role } from './role/entities/role.entity';
import { FeatureModule } from './feature/feature.module';
import { Feature } from './feature/entities/feature.entity';
import { ManufacturerModule } from './manufacturer/manufacturer.module';
import { ProductModule } from './product/product.module';
import { Product } from './product/entities/product.entity';
import { Manufacturer } from './manufacturer/entities/manufacturer.entity';
import { ColorModule } from './color/color.module';
import { OrderModule } from './order/order.module';
import { Color } from './color/entities/color.entity';
import { Order } from './order/entities/order.entity';
import { OrderItem } from './order/entities/order-item.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({ 
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'tung4865',
      database: 'BILLIARD_SHOP',
      entities: [User, Customer, Role, Feature, Manufacturer, Product, Order, Color, OrderItem],
      synchronize: true,
    }),
    UsersModule,
    AuthModule,
    CustomerModule,
    RoleModule,
    FeatureModule,
    ManufacturerModule,
    ProductModule,
    ColorModule,
    OrderModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
